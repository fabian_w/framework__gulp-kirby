# Gulp v4 / Kirby v3

- [Kirby CMS v3](https://getkirby.com) - a file‑based CMS that's 'easy to setup, easy to use & flexible as hell'
- [Gulp v4](https://gulpjs.com) - the streaming build system

## Requirements
- PHP 7.2 (or later)
- [Node.js](https://nodejs.org) 8 (or later) + NPM
- (recommended) [Node Version Manager](https://github.com/nvm-sh/nvm)

## Installation
```
npm install
```



## Usage
### Development
```
npm run start
```

### Build
```
npm run build
```

### Kirby
Go to /panel to install your first user.

## Settings
Consider removing the `/content/*` folder from .gitignore.
Live website should have a `/site/config/config.[your-domain.com].php` file to disable debug mode.

## Insights
In order to keep everything neat, each task resides in [its own file](https://gulpjs.com/docs/en/getting-started/javascript-and-gulpfiles#splitting-a-gulpfile) under `/tasks/`.

Pre-installed Kirby plugins:
- [K3 Fingerprint](https://github.com/bnomei/kirby3-fingerprint) by @bnomei
- [K3 Builder](https://github.com/TimOetting/kirby-builder) by @TimOetting

Pre-installed Javascript plugins:
- [Tiny Slider](https://github.com/ganlanyuan/tiny-slider) by @ganlany

## Example
[Burg Halle, IOS Website](http://ios.truth.design/)

## ToDo

- Explain Folders
- Explain basic Kirby
- Example Font
- Link to good Scss sources
- Link to good import / export JS sources
- Link to good basic PHP sources
- Framework like variables like $yellow as $primary
