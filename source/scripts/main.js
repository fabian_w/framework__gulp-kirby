'use strict';

// https://www.npmjs.com/package/tiny-slider
import { tns } from '../../node_modules/tiny-slider/src/tiny-slider';
import ToggleMenu from './partials/toggleMenu';

// Toggle menu init
const toggleMenu = new ToggleMenu();
toggleMenu.init();

// Tiny slider init
document.querySelectorAll('.slider').forEach((slider) => {
  tns({
    container: slider,
    controlsPosition: 'bottom',
  });
});
