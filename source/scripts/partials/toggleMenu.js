/**
 * ToggleMenu
 *
 * @module ToggleMenu
 */
export default class ToggleMenu {
  /**
   * Define default variables as properties
   *
   * @constructor
   */
  constructor() {
    this.container = document.querySelector('.nav');

    this.burgerToggle = this.container.querySelector('.burger');
    this.parentPagesMenu = this.container.querySelector(
      '.menu__pages.--parent'
    );

    this.onClickBurgerToggle = this.onClickBurgerToggle.bind(this);
  }

  /**
   * Initialize the module
   */
  init() {
    this.addEvents();
  }

  /**
   * Add event listeners
   */
  addEvents() {
    this.burgerToggle.addEventListener(
      'click',
      this.onClickBurgerToggle
    );
  }

  /**
   * On click off canvas wrapper actions
   */
  onClickBurgerToggle() {
    this.burgerToggle.classList.toggle('--open');
    this.parentPagesMenu.classList.toggle('--open');
  }
}
