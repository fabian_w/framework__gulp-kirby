/*
---------------------------------------
Assets - Images
---------------------------------------
*/

const
  {src, dest, series, parallel, lastRun} = require('gulp'),
  conf = require('../config'),

  browserSync = require('browser-sync').init,
  del = require('del'),
  {execSync} = require('child_process'),
  flatten = require('gulp-flatten')
;


/*
 * Copies images - well, that's it
 */
function copyImages() {
  const imageSource = [
    conf.src.images + '/**/*.*'
  ];

  return src(imageSource, {since: lastRun(copyImages)})
    .pipe(flatten())
    .pipe(dest(conf.dist.images))
    .pipe(browserSync.stream())
  ;
}

/*
 * Exports
 */
exports.images = copyImages;
