/*
---------------------------------------
Monitoring
---------------------------------------
*/

const
  {watch, parallel} = require('gulp'),
  conf = require('../config'),

  {styles} = require('./styles.js'),
  {scripts} = require('./scripts.js'),
  {fonts} = require('./fonts.js'),
  {images} = require('./images.js'),

  browserSync = require('browser-sync').init
;


/*
 * See https://github.com/BrowserSync/browser-sync/issues/711
 */

function reload(done) {
  browserSync.reload();
  done();
}


/*
 * Watches for changes, recompiles & injects assets
 */

function watchStyles() {
  watch(conf.src.styles + '/**/*.scss', styles);
}

function watchScripts() {
  watch(conf.src.scripts + '/**/*.js', scripts);
}

function watchFonts() {
  watch(conf.src.fonts + '/**/*', fonts);
}

function watchImages() {
  watch(conf.src.images + '/**/*', images);
}

function watchCode() {
  watch(conf.watch.code, reload);
}


/*
 * Exports
 */

exports.watch = parallel(
  watchStyles,
  watchScripts,
  watchFonts,
  watchCode,
  watchImages
);
