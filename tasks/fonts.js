/*
---------------------------------------
Assets - Fonts
---------------------------------------
*/

const
  {src, dest, series, parallel, lastRun} = require('gulp'),
  conf = require('../config'),

  browserSync = require('browser-sync').init,
  del = require('del'),
  {execSync} = require('child_process'),
  flatten = require('gulp-flatten')
;


/*
 * Copies fonts - well, that's it
 */
function copyFonts() {
  const filetypes = conf.fonts.allowed.join(',');
  const fontSource = [
    conf.src.fonts + '/**/*.{' + filetypes + '}'
  ];

  return src(fontSource, {since: lastRun(copyFonts)})
    .pipe(flatten())
    .pipe(dest(conf.dist.fonts))
    .pipe(browserSync.stream())
  ;
}

/*
 * Exports
 */
exports.fonts = copyFonts;
