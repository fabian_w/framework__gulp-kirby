<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <title><?= $site->title()->html() ?> | <?php echo $page->title()->html() ?></title>
  <meta name="description" content="<?php echo $site->description()->html() ?>">
  <meta name="keywords" content="<?php echo $site->keywords()->html() ?>">
  <meta name="keywords" content="<?php echo $site->keywords()->html() ?>">

  <?php
      if (option('environment', 'development') === 'production') {
        echo Bnomei\Fingerprint::css('/assets/custom/styles/main.min.css', ['integrity' => true]);
      } else {
        echo css('assets/custom/styles/main.css');
      }
    ?>

  <style>
    <?php echo $site->css() ?>
  </style>
</head>

<body class="layout <?php echo $page->template() ?>">

  <nav class='nav'>
    <?php snippet('menu') ?>
  </nav>

  <main class='content'>
