<?php

function walkerNavigation ($pages, $isParent = false) {
	$out = '';

	if ($isParent) {
		$out .= '<ul class="menu__pages --parent">';
	} else {
		$out .= '<ul class="menu__pages">';
	}

	foreach ($pages as $page):
		$out .= '<li ' . ($page->isActive() ? 'class="--active"' : '') . '>';
			$out .= '<a class="menu__pages__title" href="' . $page->url() . '"><span class="menu__type">' . html($page->type()) . '</span>' . html($page->title()) . '</a>';

			$children = $page->children()->listed();
			if ($children->count() > 0 && $page->isOpen()) :
				$out .= walkerNavigation($children);
			endif;
		$out .= '</li>';
	endforeach;
	$out .= '</ul>';
	return $out;
}

?>

	<div class='menu'>
		<ul class="menu__title">
			<li><a href="<?php echo $site->url() ?>"><?php echo $site->title() ?></a></li>
			<li class="burger">
				<span class="burger__line"></span>
				<span class="burger__line"></span>
				<span class="burger__line"></span>
				<span class="burger__line"></span>
			</li>
		</ul>

		<?php $items = $pages->filter(function ($selection) {
			return $selection->template() == 'default';
		})->listed();
    print_r($page->children);
		if ($items->count() > 0) :
			echo walkerNavigation($items, $isParent = true);
		endif ?>
	</div>
