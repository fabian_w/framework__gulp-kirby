<section class='images images--<?= $data->style() ?>'>
  <?php if ($data->style() == 'slider'): ?>
    <div class='slider'>
  <?php endif ?>

  <?php
    $images =  $data->images()->toFiles();
    foreach($images as $image): ?>
      <div>
        <figure>
          <img src="<?= $image->url() ?>" alt="<?= $image->alt() ?>">
          <?php if ($image->alt()): ?>
          <figcaption><?= $image->alt() ?></figcaption>
          <?php endif; ?>
        </figure>
      </div>
	<?php
    endforeach ?>

  <?php if ($data->style() === 'slider'): ?>
    </div>
  <?php endif ?>
</section>
