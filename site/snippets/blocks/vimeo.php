<section class='vimeo'>
  <div class='vimeo__wrapper'>
    <?php
    $url = $data->text();
    $urlVar = substr(parse_url($url, PHP_URL_PATH), 1);

    echo "<iframe class='vimeo__iframe' src='https://player.vimeo.com/video/{$urlVar}?color=cfcfcf&title=0&byline=0&portrait=0' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";

    ?>
  </div>
</section>
