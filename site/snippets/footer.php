</main>

<?php

  if (option('environment', 'development') === 'production') {
    echo Bnomei\Fingerprint::js('/assets/custom/scripts/main.min.js', ['integrity' => true]);
  } else {
    echo js('assets/custom/scripts/main.js');
  }

?>

</body>
</html>
