<?php snippet('header') ?>

	<h1><?= $page->title() ?></h1>
	
	<?php # /site/templates/yourtemplate.php
		foreach($page->builder()->toBuilderBlocks() as $block):
		  snippet('blocks/' . $block->_key(), array('data' => $block));
		endforeach;
	?>

<?php snippet('footer') ?>
