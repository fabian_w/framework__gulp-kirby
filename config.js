const
  src = 'source/',
  dist = 'assets/custom/',
  pkg = require('./package.json'),

  localURL = '127.0.0.1:8000'
;

module.exports = {
  src: {
    styles: src + 'styles',
    scripts: src + 'scripts',
    images: src + 'images',
    icons: src + 'images/icons',
    fonts: src + 'fonts',
  },
  dist: {
    styles: dist + 'styles',
    scripts: dist + 'scripts',
    images: dist + 'images',
    icons: dist + 'images',
    fonts: dist + 'fonts',
  },
  styles: {
    linting: {
      // For more options, see https://github.com/olegskl/gulp-stylelint#formatters
      fix: false,
      failAfterError: false,
      reporters: [{
        formatter: 'string',
        console: true,
      }],
    },
    sass: {
      // For more options, see https://github.com/sass/node-sass#options
      precision: 10, // https://github.com/sass/sass/issues/1122
      includePaths: ['node_modules'],
    },
    prefix: {
      // For more options, see https://github.com/postcss/autoprefixer#options
    },
  },
  scripts: {
    input: 'main.js', // Place it in your `src` + `scripts` directory
    linting: {}, // For more options, see https://github.com/adametry/gulp-eslint#eslintoptions
    webpack: {
      mode: 'none',
      watch: false,
    },
    babel: {
      presets: ['@babel/preset-env'],
    },
  },
  fonts: {
    allowed: ['ttf', 'woff', 'woff2'], // For example, generating from OTF without shipping source files
  },
  server: {
    enable: true,
    connect: {
      // For more options, see https://github.com/micahblu/gulp-connect-php#options
      base: '.',
      router: 'kirby/router.php',
    },
  },
  browsersync: {
    // For more options, see https://browsersync.io/docs/options
    proxy: localURL,
    port: 4000,
    notify: true,
    open: true,
    online: false,
  },
  watch: {
    code: [
      'site/**/*.{php,yml}',
      'content/**/*',
    ],
  },
  sourcemaps: {
    enable: true,
    path: '.', // This defaulfs to `dist` + `styles` & `dist` + `scripts`
  },
  subsetting: {
    // For more options, see https://github.com/filamentgroup/glyphhanger
    enable: true,
    urls: [
      // In combination with `spider: true`, this should be sufficient:
      localURL, // Browsersync source
      // Otherwise, add as many local/external files & URLs as you like:
      // pkg.homepage,
      // 'http://example.com',
      // './example.html'
    ],
    formats: ['woff'], // Available formats: 'ttf', 'woff', 'woff-zopfli', 'woff2'
    spider: false,
    whitelist: false,
    us_ascii: false,
    latin: false,
    output_css: true,
    css_selector: false,
  },
};
